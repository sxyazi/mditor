import React, { Component } from 'react'
import Plain from 'slate-plain-serializer'

import { Editor } from 'slate-react'

import MarkdownPlugin from './md'
import MarkdownComponents from './md/components'
import EditList from './md/packages/slate-edit-list/src/slate-edit-list'
import { DEFAULT_EDITLIST } from './md/constants/defaults'

class Mditor extends Component {
  constructor(props) {
    super(props)

    this.plugins = [
      MarkdownPlugin(),
      EditList(DEFAULT_EDITLIST),
      ...MarkdownComponents
    ]
    this.state = { value: Plain.deserialize('- xxx') }
  }

  onChange = ({ value }) => {
    this.setState({ value })
  }

  render() {
    return (
      <div>
        <Editor
          value={this.state.value}
          plugins={this.plugins}
          onChange={this.onChange}
        />
      </div>
    )
  }
}

export default Mditor
