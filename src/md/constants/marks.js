export const BOLD = 'BOLD'
export const ITALIC = 'ITALIC'
export const BOLDITALIC = 'BOLDITALIC'

export const CODE = 'CODE'
export const STRIKETHROUGH = 'STRIKETHROUGH'
export const UNDERLINE = 'UNDERLINE'

// mark styles
export const FONTBGCOLOR = 'FONTBGCOLOR'
export const FONTCOLOR = 'FONTCOLOR'
export const FONTSIZE = 'FONTSIZE'
export const LETTERSPACING = 'LETTERSPACING'
