export const DOCUMENT = 'document'
export const TEXT = 'unstyled'

// Classic blocks
export const CODE = 'code_block'
export const CODE_LINE = 'code_line'
export const BLOCKQUOTE = 'blockquote'
export const PARAGRAPH = 'paragraph'
export const FOOTNOTE = 'footnote'
export const HTML = 'html_block'
export const HR = 'hr'

// Headings
export const HEADING_1 = 'header_one'
export const HEADING_2 = 'header_two'
export const HEADING_3 = 'header_three'
export const HEADING_4 = 'header_four'
export const HEADING_5 = 'header_five'
export const HEADING_6 = 'header_six'

// Table
export const TABLE = 'table'
export const TABLE_ROW = 'table_row'
export const TABLE_CELL = 'table_cell'

// Lists
export const OL_LIST = 'ordered_list'
export const UL_LIST = 'unordered_list'
export const LIST_ITEM = 'list_item'

// Default block
export const DEFAULT = 'paragraph'

// Special
export const IMAGE = 'image'
export const VIDEO = 'video'
