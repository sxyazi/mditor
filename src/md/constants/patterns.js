export const REGEX_ITALIC = /(\*|_)((?!\1).)+?\1(?:(?!\*|_)|(?=$|\s|\n))/g
export const REGEX_BOLD = /(\*\*|__)((?!\1).)+?\1(?:(?!\*|_)|(?=$|\s|\n))/g
export const REGEX_BOLDITALIC = /(\*\*\*|___)((?!\1).)+?\1(?:(?!\*|_)|(?=$|\s|\n))/g
