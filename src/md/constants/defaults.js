import { OL_LIST, UL_LIST, LIST_ITEM, PARAGRAPH } from './blocks'

export const DEFAULT_EDITLIST = {
  types: [OL_LIST, UL_LIST],
  typeItem: LIST_ITEM,
  typeDefault: PARAGRAPH,
  ordered: true
}
