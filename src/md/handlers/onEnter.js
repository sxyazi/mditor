// matches
import matchHeader from '../matches/header'
import matchList from '../matches/list'

// blocks
import { CODE, CODE_LINE, PARAGRAPH } from '../constants/blocks'

export default change => {
  const { value } = change
  const currentTextNode = value.texts.get(0)
  const getCurrentBlock = value.blocks.get(0)
  const currentLineText = currentTextNode.text
  let matched

  if ((matched = currentLineText.match(/(^\s*)#{1,6}\s/m))) {
    matchHeader(currentTextNode, matched, change)
  } else if ((matched = currentLineText.match(/((?:^\s*)(?:[*+-]\s))/m))) {
    matchList(currentTextNode, matched, change)
  } else if ((matched = currentLineText.match(/((?:^\s*)(?:\d+\.\s))/m))) {
    matchList(currentTextNode, matched, change, true)
  }

  if (
    getCurrentBlock.type !== CODE &&
    getCurrentBlock.type !== CODE_LINE &&
    currentLineText.length <= value.selection.focusOffset
  )
    return change.insertBlock(PARAGRAPH)
}
