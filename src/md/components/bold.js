import * as React from 'react'
import { Range } from 'slate'
import { ITALIC, BOLD } from '../constants/marks'
import { ASTERISK_BOLD } from '../constants/symbols'
import removeMark from '../utils/remove-mark'
import removeallMark from '../utils/removeall-mark'
import { REGEX_BOLD } from '../constants/patterns'

const state = {
  currentMark: null
}

const isBold = mark => mark && mark.type === BOLD
const isCurrentBold = mark =>
  isBold(mark) && mark.data.get('n') === state.currentMark

const decorateNode = (node, editor, next) => {
  const decorations = []
  const currentTextNode = node.getFirstText()

  let matched
  while ((matched = REGEX_BOLD.exec(currentTextNode.text))) {
    const matchedLength = matched[0].length

    decorations.push({
      anchor: {
        key: currentTextNode.key,
        offset: matched.index
      },
      focus: {
        key: currentTextNode.key,
        offset: matched.index + 2
      },
      mark: {
        type: ASTERISK_BOLD
      }
    })

    decorations.push({
      anchor: {
        key: currentTextNode.key,
        offset: matched.index + matchedLength - 2
      },
      focus: {
        key: currentTextNode.key,
        offset: matched.index + matchedLength
      },
      mark: {
        type: ASTERISK_BOLD
      }
    })
  }

  return [...decorations, ...(next() || [])]
}

export default {
  onChange(change, next) {
    const oper = change.operations.first()
    const operMark = oper.value.marks.first()

    // hide decoration when wrapping
    if (oper.type === 'split_node' && isCurrentBold(operMark)) {
      state.currentMark = null
      return change
        .command(removeallMark)
        .setDecorations(
          decorateNode(oper.value.blocks.first(), change, () => {})
        )
    }

    // update current mark
    let currentMark
    if ((currentMark = change.value.marks.first())) {
      state.currentMark = currentMark.data.get('n')
    } else {
      state.currentMark = null
    }

    // hide decoration when wrapping
    if (
      oper.type === 'set_selection' &&
      isBold(operMark) &&
      !isCurrentBold(currentMark)
    ) {
      state.currentMark = null
      return change.setDecorations(
        decorateNode(oper.value.blocks.first(), change, () => {})
      )
    }

    change.setDecorations([])
    return next()
  },

  onSelect(e, change, next) {
    let matched

    change.value.texts.forEach(textNode => {
      removeMark(change, textNode, BOLD)

      while ((matched = REGEX_BOLD.exec(textNode.text))) {
        const matchedLength = matched[0].length

        removeMark(
          change,
          Range.create({
            anchor: {
              key: textNode.key,
              offset: matched.index
            },
            focus: {
              key: textNode.key,
              offset: matched.index + matchedLength
            }
          }),
          ITALIC
        )

        change.addMarkAtRange(
          Range.create({
            anchor: {
              key: textNode.key,
              offset: matched.index
            },
            focus: {
              key: textNode.key,
              offset: matched.index + matchedLength
            }
          }),
          {
            type: BOLD,
            data: { n: Math.random() }
          }
        )
      }
    })

    return next()
  },

  renderMark(props, editor, next) {
    switch (props.mark.type) {
      case BOLD:
        return <strong {...props.attributes}>{props.children}</strong>
      case ASTERISK_BOLD:
        return (
          <span
            style={{
              color: 'gray',
              display: isCurrentBold(props.marks.first()) ? 'inline' : 'none'
            }}
          >
            {props.children}
          </span>
        )
      default:
        return next()
    }
  },

  decorateNode
}
