import * as React from 'react'
import { Range } from 'slate'
import { ITALIC, BOLD, BOLDITALIC } from '../constants/marks'
import { ASTERISK_BOLDITALIC } from '../constants/symbols'
import removeMark from '../utils/remove-mark'
import removeallMark from '../utils/removeall-mark'
import { REGEX_BOLDITALIC } from '../constants/patterns'

const state = {
  currentMark: null
}

const isBoldItalic = mark => mark && mark.type === BOLDITALIC
const isCurrentBoldItalic = mark =>
  isBoldItalic(mark) && mark.data.get('n') === state.currentMark

const decorateNode = (node, editor, next) => {
  const decorations = []
  const currentTextNode = node.getFirstText()

  let matched
  while ((matched = REGEX_BOLDITALIC.exec(currentTextNode.text))) {
    const matchedLength = matched[0].length

    decorations.push({
      anchor: {
        key: currentTextNode.key,
        offset: matched.index
      },
      focus: {
        key: currentTextNode.key,
        offset: matched.index + 3
      },
      mark: {
        type: ASTERISK_BOLDITALIC
      }
    })

    decorations.push({
      anchor: {
        key: currentTextNode.key,
        offset: matched.index + matchedLength - 3
      },
      focus: {
        key: currentTextNode.key,
        offset: matched.index + matchedLength
      },
      mark: {
        type: ASTERISK_BOLDITALIC
      }
    })
  }

  return [...decorations, ...(next() || [])]
}

export default {
  onChange(change, next) {
    const oper = change.operations.first()
    const operMark = oper.value.marks.first()

    // hide decoration when wrapping
    if (oper.type === 'split_node' && isCurrentBoldItalic(operMark)) {
      state.currentMark = null
      return change
        .command(removeallMark)
        .setDecorations(
          decorateNode(oper.value.blocks.first(), change, () => {})
        )
    }

    // update current mark
    let currentMark
    if ((currentMark = change.value.marks.first())) {
      state.currentMark = currentMark.data.get('n')
    } else {
      state.currentMark = null
    }

    // hide decoration when wrapping
    if (
      oper.type === 'set_selection' &&
      isBoldItalic(operMark) &&
      !isCurrentBoldItalic(currentMark)
    ) {
      state.currentMark = null
      return change.setDecorations(
        decorateNode(oper.value.blocks.first(), change, () => {})
      )
    }

    change.setDecorations([])
    return next()
  },

  onSelect(e, change, next) {
    let matched

    change.value.texts.forEach(textNode => {
      removeMark(change, textNode, BOLDITALIC)

      while ((matched = REGEX_BOLDITALIC.exec(textNode.text))) {
        const matchedLength = matched[0].length

        removeMark(
          change,
          Range.create({
            anchor: {
              key: textNode.key,
              offset: matched.index
            },
            focus: {
              key: textNode.key,
              offset: matched.index + matchedLength
            }
          }),
          [BOLD, ITALIC]
        )

        change.addMarkAtRange(
          Range.create({
            anchor: {
              key: textNode.key,
              offset: matched.index
            },
            focus: {
              key: textNode.key,
              offset: matched.index + matchedLength
            }
          }),
          {
            type: BOLDITALIC,
            data: { n: Math.random() }
          }
        )
      }
    })

    return next()
  },

  renderMark(props, editor, next) {
    switch (props.mark.type) {
      case BOLDITALIC:
        return (
          <em {...props.attributes}>
            <strong>{props.children}</strong>
          </em>
        )
      case ASTERISK_BOLDITALIC:
        return (
          <span
            style={{
              color: 'gray',
              display: isCurrentBoldItalic(props.marks.first())
                ? 'inline'
                : 'none'
            }}
          >
            {props.children}
          </span>
        )
      default:
        return next()
    }
  },

  decorateNode
}
