import Node from '../elements/node'
import { OL_LIST, UL_LIST, LIST_ITEM } from '../constants/blocks'

export default {
  renderNode(props, editor, next) {
    switch (props.node.type) {
      case OL_LIST:
        return Node('ol', {})(props)
      case UL_LIST:
        return Node('ul', {})(props)
      case LIST_ITEM:
        return Node('li', {})(props)
      default:
        return next()
    }
  }
}
