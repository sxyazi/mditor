import Node from '../elements/node'
import {
  HEADING_1,
  HEADING_2,
  HEADING_3,
  HEADING_4,
  HEADING_5,
  HEADING_6
} from '../constants/blocks'

export default {
  onKeyDown(e, change, next) {
    if (e.key === 'Enter') {
      const { value } = change
      const { type } = value.blocks.get(0)

      switch (type) {
        case HEADING_1:
        case HEADING_2:
        case HEADING_3:
        case HEADING_4:
        case HEADING_5:
        case HEADING_6:
          return change.splitBlock().setBlocks('paragraph')
        default:
      }
    }

    return next()
  },

  renderNode(props, editor, next) {
    switch (props.node.type) {
      case HEADING_1:
        return Node('h1', {})(props)
      case HEADING_2:
        return Node('h2', {})(props)
      case HEADING_3:
        return Node('h3', {})(props)
      case HEADING_4:
        return Node('h4', {})(props)
      case HEADING_5:
        return Node('h5', {})(props)
      case HEADING_6:
        return Node('h6', {})(props)
      default:
        return next()
    }
  }
}
