import Header from './header'

import Bold from './bold'
import Italic from './italic'
import BoldItalic from './boldItalic'

import List from './list'

export default [Italic, Bold, BoldItalic, Header, List]
