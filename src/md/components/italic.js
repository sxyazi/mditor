import * as React from 'react'
import { Range } from 'slate'
import { ITALIC } from '../constants/marks'
import { ASTERISK_ITALIC } from '../constants/symbols'
import removeMark from '../utils/remove-mark'
import removeallMark from '../utils/removeall-mark'
import { REGEX_ITALIC } from '../constants/patterns'

const state = {
  currentMark: null
}

const isItalic = mark => mark && mark.type === ITALIC
const isCurrentItalic = mark =>
  isItalic(mark) && mark.data.get('n') === state.currentMark

const decorateNode = (node, editor, next) => {
  const decorations = []
  const currentTextNode = node.getFirstText()

  let matched
  while ((matched = REGEX_ITALIC.exec(currentTextNode.text))) {
    const matchedLength = matched[0].length

    decorations.push({
      anchor: {
        key: currentTextNode.key,
        offset: matched.index
      },
      focus: {
        key: currentTextNode.key,
        offset: matched.index + 1
      },
      mark: {
        type: ASTERISK_ITALIC
      }
    })

    decorations.push({
      anchor: {
        key: currentTextNode.key,
        offset: matched.index + matchedLength - 1
      },
      focus: {
        key: currentTextNode.key,
        offset: matched.index + matchedLength
      },
      mark: {
        type: ASTERISK_ITALIC
      }
    })
  }

  return [...decorations, ...(next() || [])]
}

export default {
  onChange(change, next) {
    const oper = change.operations.first()
    const operMark = oper.value.marks.first()

    // hide decoration when wrapping
    if (oper.type === 'split_node' && isCurrentItalic(operMark)) {
      state.currentMark = null
      return change
        .command(removeallMark)
        .setDecorations(
          decorateNode(oper.value.blocks.first(), change, () => {})
        )
    }

    // update current mark
    let currentMark
    if ((currentMark = change.value.marks.first())) {
      state.currentMark = currentMark.data.get('n')
    } else {
      state.currentMark = null
    }

    // hide decoration when wrapping
    if (
      oper.type === 'set_selection' &&
      isItalic(operMark) &&
      !isCurrentItalic(currentMark)
    ) {
      state.currentMark = null
      return change.setDecorations(
        decorateNode(oper.value.blocks.first(), change, () => {})
      )
    }

    change.setDecorations([])
    return next()
  },

  onSelect(e, change, next) {
    let matched

    change.value.texts.forEach(textNode => {
      removeMark(change, textNode, ITALIC)

      while ((matched = REGEX_ITALIC.exec(textNode.text))) {
        change.addMarkAtRange(
          Range.create({
            anchor: {
              key: textNode.key,
              offset: matched.index
            },
            focus: {
              key: textNode.key,
              offset: matched.index + matched[0].length
            }
          }),
          {
            type: ITALIC,
            data: { n: Math.random() }
          }
        )
      }
    })

    return next()
  },

  renderMark(props, editor, next) {
    switch (props.mark.type) {
      case ITALIC:
        return <em {...props.attributes}>{props.children}</em>
      case ASTERISK_ITALIC:
        return (
          <span
            style={{
              color: 'gray',
              display: isCurrentItalic(props.marks.first()) ? 'inline' : 'none'
            }}
          >
            {props.children}
          </span>
        )
      default:
        return next()
    }
  },

  decorateNode
}
