import {
  Range,
  Point
} from 'slate'

import {
  HEADING_1,
  HEADING_2,
  HEADING_3,
  HEADING_4,
  HEADING_5,
  HEADING_6
} from '../constants/blocks'

export default (currentTextNode, matched, change) => {
  const matchedLength = matched[0].length
  const count = (matched[0].match(/#/g) || []).length
  let header

  if (count === 1) header = HEADING_1
  else if (count === 2) header = HEADING_2
  else if (count === 3) header = HEADING_3
  else if (count === 4) header = HEADING_4
  else if (count === 5) header = HEADING_5
  else if (count === 6) header = HEADING_6
  else return

  return change.setBlocks(header).deleteAtRange(
    Range.create({
      anchor: Point.create({
        key: currentTextNode.key,
        offset: matched.index
      }),
      focus: Point.create({
        key: currentTextNode.key,
        offset: matched.index + matchedLength
      })
    })
  )
}