import { Range, Point } from 'slate'
import EditList from '../packages/slate-edit-list/src/slate-edit-list'
import { DEFAULT_EDITLIST } from '../constants/defaults'

export default (currentTextNode, matched, change, ordered = false) => {
  change.deleteAtRange(
    Range.create({
      anchor: Point.create({
        key: currentTextNode.key,
        offset: matched.index
      }),
      focus: Point.create({
        key: currentTextNode.key,
        offset: matched.index + matched[0].length
      })
    })
  )

  const options = Object.assign({}, DEFAULT_EDITLIST, { ordered })
  const currentType = ordered ? options.types[0] : options.types[1]

  const { utils, changes } = EditList(options)
  if (utils.isSelectionInList(change.value)) {
    if (utils.getCurrentList(change.value).type !== currentType) {
      return changes.wrapInList(change, currentType)
    } else {
      return changes.unwrapList(change)
    }
  } else {
    return changes.wrapInList(change, currentType)
  }
}
