import { Range } from 'slate'

const array = v => (v instanceof Array ? v : [v])

export default (change, textNode, markType = null) => {
  const range =
    textNode instanceof Range
      ? textNode
      : Range.create({
          anchor: {
            key: textNode.key,
            offset: 0
          },
          focus: {
            key: textNode.key,
            offset: textNode.text.length
          }
        })

  change.value.document.getMarksAtRange(range).some(mark => {
    if (!markType || array(markType).indexOf(mark.type) !== -1) {
      change.removeMarkAtRange(range, {
        type: mark.type,
        data: mark.data
      })
    }

    return void 0
  })
}
