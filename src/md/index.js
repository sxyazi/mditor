import onEnter from './handlers/onEnter'
import { KEY_ENTER } from './constants/keys'

export default () => {
  return {
    onKeyUp(e, change, next) {
      return next()
    },

    onKeyDown(e, change, next) {
      switch (e.key) {
        case KEY_ENTER:
          onEnter(change)
          break

        default:
      }

      return next()
    }
  }
}
