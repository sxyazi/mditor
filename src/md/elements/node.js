import * as React from 'react'
import { mapValues } from 'lodash'

export default (Tag, stylesAttr) => {
  const NodeComponent = ({ attributes, children, node }) => {
    return (
      <Tag
        {...attributes}
        data-slate-type={Tag}
        style={mapValues(stylesAttr, val => val && val(node))}
      >
        {children}
      </Tag>
    )
  }

  NodeComponent.displayName = `${Tag}-node`

  return NodeComponent
}
